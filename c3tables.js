(function ($) {
  Drupal.behaviors.c3tables = {
    attach: function (context, settings) {

      $('.field-name-field-charts').each(function() {
        var $this = $(this);


        var chartType = $this.find('.field-name-field-chart-type').html().trim();
        var dataTable = $this.find('.tablefield');



        $this.find('.chart-wrapper').tableToD3Chart({
          chartDataTable: dataTable,
          chartOptions: {
            type: chartType
          }
        });

      });
    }
  };


  $.fn.tableToD3Chart = function (options) {

    var defaults = {
      wrapper: '#' + $(this).attr('id'),
      chartTitleWrapper: "caption",
      useDom: true,
      data: [],
      chartLabel: "Chart Label"
    }

    var o = $.extend(defaults, options);

    var getChartData = {

      title: function (target) {
        if (o.useDom) {
          var $this = $(target),
            caption = $this.find(o.chartTitleWrapper).text();
          return caption;
        }
        else {
          var caption = o.chartLabel;
          return caption;
        }

      },

      tableData: function (target) {
        if (o.useDom) {
          var $this = $(target),
            $rows = $this.find("tr").not(":first"),
            rowData = [];

          $rows.each(function () {
            var $dataCells = $(this).children(),
              tmpData = [];
            $dataCells.each(function () {
              tmpData.push($(this).text());
            });
            rowData.push(tmpData)
          });

          return rowData;
        }
        else {
          return o.data;
        }
      } // tableData
    }

    // Set default chart options.
    var chartOptions = {
      bindto: o.wrapper,
      data: {
        columns: getChartData.tableData(o.chartDataTable),
        type: o.chartOptions.type
      },
      donut: {
        title: getChartData.title(o.chartDataTable)
      }
    };

    // Extend defaults with the options passed in.
    $.extend(true, chartOptions, o.chartOptions);

    var chart = c3.generate(chartOptions);

  }; // $.fn


})(jQuery);