<?php
/**
 * @file
 * c3tables.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function c3tables_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_chart_data'
  $field_bases['field_chart_data'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_chart_data',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'tablefield',
    'settings' => array(
      'cell_processing' => 0,
      'export' => 0,
      'lock_values' => 0,
      'restrict_rebuild' => 0,
    ),
    'translatable' => 0,
    'type' => 'tablefield',
  );

  // Exported field_base: 'field_chart_type'
  $field_bases['field_chart_type'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_chart_type',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'line' => 'line',
        'spline' => 'spline',
        'step' => 'step',
        'area' => 'area',
        'area-spline' => 'area-spline',
        'area-step' => 'area-step',
        'bar' => 'bar',
        'scatter' => 'scatter',
        'pie' => 'pie',
        'donut' => 'donut',
        'gauge' => 'gauge',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
